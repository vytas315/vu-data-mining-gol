from setuptools import setup

setup(
    name='reverse_gol_solver',
    version='0.1',
    description='Utility to train reverse GOL model',
    author='Vytas Bradunas',
    py_modules=['reverse_gol_solver'],
    install_requires=[
        'Click',
    ],
    setup_requires=['pytest-runner', 'flake8', 'plotnine'],
    tests_require=['pytest'],
    entry_points='''
        [console_scripts]
        golsolver=reverse_gol_solver.main:cli
    ''',
)
