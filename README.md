# Reverse GOL Solver

## Setup

This project uses Torch and therefore will use Conda for package management.

1) Create a Conda environment and load dependencies using

```console
> conda create --name reverse_gol_solver_env
> conda activate reverse_gol_solver_env
> conda env update
```

2) Install pip and load the package under development

```console
> conda install pip
> pip install -e .
```

Eventually it will be possible to create a wheel for the package and it will be loaded from a package manager.

## Running the Package

The CLI is documented using docstrings compiled by the Click package. Current main usage is for board generation and analysis. For example, the following command generates 3x3 boards, shows the 48th starting board and summary statistics for the entire dataset.

```console
> golsolver render-test-board 48 start 3 --verbose
```

## Development Notes

When adding new dependencies, export an updated yml environment file.

```console
> conda env export > environment.yml
```

## Naming

### Start and end states

Working on reverse solving can get confusing about what is the start and what is the end state. I will always use the frame of reference of forward time. Therefore, the start is t = 0 and end state can be anything after that, in most cases t = 1. In this frame, the input to the solver will be end states and will try to predict start states.
