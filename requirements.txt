certifi==2020.11.8
click==7.1.2
cycler==0.10.0
descartes==1.1.0
flake8 @ file:///home/conda/feedstock_root/build_artifacts/flake8_1601874335748/work
importlib-metadata @ file:///home/conda/feedstock_root/build_artifacts/importlib-metadata_1602263269022/work
kiwisolver @ file:///Users/runner/miniforge3/conda-bld/kiwisolver_1604323069108/work
matplotlib @ file:///Users/runner/miniforge3/conda-bld/matplotlib-suite_1605180218063/work
mccabe==0.6.1
mizani==0.7.2
numpy @ file:///Users/runner/miniforge3/conda-bld/numpy_1604946213976/work
olefile @ file:///home/conda/feedstock_root/build_artifacts/olefile_1602866521163/work
palettable==3.3.0
pandas @ file:///opt/concourse/worker/volumes/live/f14cf8c4-c564-4eff-4b17-158e90dbf88a/volume/pandas_1602088128240/work
patsy==0.5.1
pickle-mixin==1.0.2
Pillow @ file:///Users/runner/miniforge3/conda-bld/pillow_1604748707307/work
plotnine @ file:///home/conda/feedstock_root/build_artifacts/plotnine_1596743375470/work
pycodestyle @ file:///home/conda/feedstock_root/build_artifacts/pycodestyle_1589305246696/work
pyflakes==2.2.0
pyparsing==2.4.7
python-dateutil==2.8.1
pytz @ file:///home/conda/feedstock_root/build_artifacts/pytz_1604321279890/work
-e git+https://gitlab.com/vytas315/vu-data-mining-gol.git@315ac7900f8c3d8f66966a435e56d9f1507feb1e#egg=reverse_gol_solver
scipy @ file:///Users/runner/miniforge3/conda-bld/scipy_1604305021986/work
six @ file:///home/conda/feedstock_root/build_artifacts/six_1590081179328/work
statsmodels @ file:///Users/runner/miniforge3/conda-bld/statsmodels_1605985682916/work
tornado @ file:///Users/runner/miniforge3/conda-bld/tornado_1604105041409/work
zipp @ file:///home/conda/feedstock_root/build_artifacts/zipp_1603668650351/work
