from plotnine import *
import pandas


def build_reverse_hist(data):
    hist = {}
    for board in data.get('end'):
        if board in hist:
            hist[board] += 1
        else:
            hist[board] = 1
    hist_2nd_order = {}
    for board, frequency in hist.items():
        if frequency in hist_2nd_order:
            hist_2nd_order[frequency] += 1
        else:
            hist_2nd_order[frequency] = 1

    board_len = len(data.get('end')[0])
    hist_df = pandas.DataFrame(list(hist.items()), columns=['board', 'board_frequency'])
    # print('Most common boards: ', hist_df[hist_df["board_frequency"] > 1])
    print(hist_2nd_order)
    print('Unique boards: ', hist_df[hist_df["board_frequency"] == 1]['board_frequency'].sum())
    print('Non-unique boards: ', hist_df[hist_df["board_frequency"] > 1]['board_frequency'].sum())
    print('All zeros: ', hist_df[hist_df["board"] == '0' * board_len]['board_frequency'].sum())
    print('All ones: ', hist_df[hist_df["board"] == '1' * board_len]['board_frequency'].sum())
    print('Total boards: ', hist_df['board_frequency'].sum())
    chart = (
        ggplot(hist_df[(hist_df["board_frequency"] <= 600) & (hist_df["board_frequency"] > 4)])
        + aes(x='board_frequency')
        # + xlim(0, 600)
        # + ylim(0, 250)
        + geom_histogram(bins=10)
        + labs(title='Frequency of common board states')
    )
    print(chart)
