import math
import torch
from torch.utils.data import DataLoader, TensorDataset
import pickle


def scale_model(model, model_input_size, model_output_size, scaled_model_size):
    validation_data = _get_data(scaled_model_size)
    sum_wins = 0
    sum_total = 0
    for xb, yb in validation_data:
        predicted_xb = _apply_model(model, xb, model_input_size, model_output_size, scaled_model_size)
        wins, total = _get_accuracy(predicted_xb, yb)
        sum_wins += wins
        sum_total += total
        accuracy = sum_wins.float()/sum_total
        print('Scaled model accuracy: {accuracy}'.format(accuracy=accuracy))


def _get_data(scaled_model_size):
    data = _load_data_pickles(scaled_model_size)
    # data = _load_data_pickles_reverse()
    # Forward prediction for now
    valid_ds = TensorDataset(data[0], data[1])  # starting states, ending states
    return DataLoader(valid_ds, batch_size=10)


def _load_data_pickles(scaled_model_size):
    scaled_model_evaluation_size = scaled_model_size - 2
    ending_validation_file = 'ending_states_validation'
    starting_validation_file = 'starting_states_validation'
    ending_validation_file = _append_board_size_to_file(ending_validation_file, scaled_model_size)
    starting_validation_file = _append_board_size_to_file(starting_validation_file, scaled_model_size)
    with open(ending_validation_file, 'rb') as pickle_end_file:
        np_ending_validation = pickle.load(pickle_end_file)
    with open(starting_validation_file, 'rb') as pickle_start_file:
        np_starting_validation = pickle.load(pickle_start_file)
    np_ending_validation, np_starting_validation = map(
        torch.tensor, (np_ending_validation, np_starting_validation))
    np_ending_validation = np_ending_validation.view(
        -1, scaled_model_size, scaled_model_size)[
            :,
            1:1 + scaled_model_evaluation_size,
            1:1 + scaled_model_evaluation_size].reshape(-1, pow(scaled_model_evaluation_size, 2))
    return (np_starting_validation.float(), np_ending_validation.float())


def _append_board_size_to_file(file_name, model_size):
    board_size = str(model_size)
    return './data/' + file_name + '_' + board_size + 'x' + board_size + '.pickle'


def _apply_model(model, xb, model_input_size, model_output_size, scaled_model_size):
    # x, y are indexes in the scaled full model. Start at index 1 so that we have padding
    output_boards = []
    for board in xb:
        multiplier = math.ceil(1.0 * (scaled_model_size - 2) / model_output_size)
        padded_board_size = multiplier * model_output_size + 2
        padded_board = torch.zeros([padded_board_size, padded_board_size])
        padded_board[0:scaled_model_size, 0:scaled_model_size] = board.view(scaled_model_size, scaled_model_size)
        output_board = torch.zeros([scaled_model_size - 2, scaled_model_size - 2])
        for x in range(1, scaled_model_size - 1, model_output_size):
            for y in range(1, scaled_model_size - 1, model_output_size):
                x_start_index = x - 1
                x_end_index = x_start_index + model_input_size
                y_start_index = y - 1
                y_end_index = y_start_index + model_input_size
                data_to_evaluate = padded_board[
                    x_start_index: x_end_index,
                    y_start_index: y_end_index]
                output = model(data_to_evaluate.view(1, 1, model_input_size, model_input_size))
                target_square = output_board[x - 1: x - 1 + model_output_size, y - 1: y - 1 + model_output_size]
                output_board[
                    x - 1: x - 1 + model_output_size,
                    y - 1: y - 1 + model_output_size] = output.view(
                        model_output_size, model_output_size)[
                            0: target_square.shape[0],
                            0: target_square.shape[1]]  # handle edge cases where we have more data than we need
        output_boards.append(output_board)
    return torch.cat(output_boards)

                


    # Loop through all scaled model grids and generate output
        # Handle edge cases of grids falling off the end
    # return scaled down size of scaled model size


def _get_accuracy(predicted_xb, yb):
    return sum(((predicted_xb.view(-1) >= 0.5).int() == yb.view(-1)).int()), len(yb.view(-1))
