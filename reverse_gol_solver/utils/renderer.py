import math


def render_board(board):
    size = len(board)
    row_count = int(math.sqrt(size))
    for row_index in range(0, row_count):
        start_index = row_index * row_count
        row_vals = []
        for column_index in range(0, row_count):
            row_vals.append(str(int(board[start_index + column_index])))
        print(' '.join(row_vals))
