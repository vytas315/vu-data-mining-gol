import csv
import math
import numpy as np
import pickle
import random


dataset = None


def get_dataset(board_width, warm_up=5):
    """Generate localized dataset of 3x3 grids

    Generates all varieties of 3x3 grids and steps them by one
    evolution into the future. Random grids are the start states,
    the results of one evolution are the ends states. The format is:

    {
        start: [
            [0,0,0,0,0,0,0,0,0],
            [0,0,0,0,1,0,0,0,0],
            ...
        ],
        end: [
            [0,0,0,0,0,0,0,0,0],
            [0,0,0,0,0,0,0,0,0],
            ...
        ]
    }

    Since we will most often be looping through either start or end
    states, I've listed them together in arrays. Easy enough to lookup
    corresponding start and end states by matching indexes.
    """
    global dataset
    if dataset:  # Generating the dataset as a singleton
        return dataset
    print('Generating starting states...')
    _generate_start_states(board_width)
    print('Evolving states...')
    
    _evolve_start_states()
    return dataset


def save_training_data(board_width, dataset_size, warm_up=5):
    _generate_random_start_states(board_width, dataset_size)
    for warm_up_index in range(warm_up):
        print('Warming up {}...'.format(warm_up_index))
        _evolve_start_states()
        dataset['start'] = dataset['end']
    _evolve_start_states()
    _filter_empty_states()
    _save_starting_states(board_width)
    _save_ending_states(board_width)

    _generate_random_start_states(board_width, dataset_size)
    for warm_up_index in range(warm_up):
        print('Warming up {}...'.format(warm_up_index))
        _evolve_start_states()
        dataset['start'] = dataset['end']
    _evolve_start_states()
    _filter_empty_states()
    _save_starting_states_validation(board_width)
    _save_ending_states_validation(board_width)


def _save_starting_states(board_size):
    global dataset
    arrayed_data = np.array([list(row) for row in dataset.get('start')]).astype(int)
    with open('./data/starting_states_{board_size}x{board_size}.pickle'.format(board_size=board_size), 'wb') as pickle_file:
        pickle.dump(arrayed_data, pickle_file)


def _save_ending_states(board_size):
    global dataset
    arrayed_data = np.array([list(row) for row in dataset.get('end')]).astype(int)
    with open('./data/ending_states_{board_size}x{board_size}.pickle'.format(board_size=board_size), 'wb') as pickle_file:
        pickle.dump(arrayed_data, pickle_file)


def _save_starting_states_validation(board_size):
    global dataset
    arrayed_data = np.array([list(row) for row in dataset.get('start')]).astype(int)
    with open('./data/starting_states_validation_{board_size}x{board_size}.pickle'.format(board_size=board_size), 'wb') as pickle_file:
        pickle.dump(arrayed_data, pickle_file)


def _save_ending_states_validation(board_size):
    global dataset
    arrayed_data = np.array([list(row) for row in dataset.get('end')]).astype(int)
    with open('./data/ending_states_validation_{board_size}x{board_size}.pickle'.format(board_size=board_size), 'wb') as pickle_file:
        pickle.dump(arrayed_data, pickle_file)


def _generate_start_states(board_width=3):
    global dataset
    starting_boards = []
    variations = int(math.pow(2, math.pow(board_width, 2)))
    board_size = int(math.pow(board_width, 2))
    binary_specifier = '0' + str(board_size) + 'b'
    for board_decimal in range(0, variations):
        board_binary = f'{board_decimal:{binary_specifier}}'
        starting_boards.append(board_binary)
    dataset = {}
    dataset['start'] = starting_boards


def _generate_random_start_states(board_width, dataset_size):
    if board_width > 3:
        _generate_random_start_states_alternate(board_width, dataset_size)
    elif (board_width == 3):
        _generate_start_states()
    else:
        global dataset
        starting_boards = []
        board_size = int(math.pow(board_width, 2))
        variations = int(math.pow(2, math.pow(board_width, 2)))
        binary_specifier = '0' + str(board_size) + 'b'
        for board_index in range(0, dataset_size):
            board_decimal = round(random.random() * (variations - 1))
            board_binary = f'{board_decimal:{binary_specifier}}'
            starting_boards.append(board_binary)
        dataset = {}
        dataset['start'] = starting_boards


def _generate_random_start_states_alternate(board_width, dataset_size):
    global dataset
    starting_boards = []
    board_size = int(math.pow(board_width, 2))
    for board_index in range(0, dataset_size):
        board_binary = np.random.randint(0, 2, size=board_size)
        starting_boards.append(board_binary)
    dataset = {}
    dataset['start'] = starting_boards


def _evolve_start_states():
    global dataset
    dataset['end'] = []
    for board in dataset.get('start'):
        dataset['end'].append(_evolve_board(board))


def _evolve_board(board, no_wrap=False):
    row_length = int(math.sqrt(len(board)))
    if no_wrap:
        row_length -= 1
    start_index = 1 if no_wrap else 0
    new_board = []
    for y_index in range(start_index, row_length):
        for x_index in range(start_index, row_length):
            new_board.append(_evolve_cell(board, x_index, y_index))
    return ''.join(new_board)


def _evolve_cell(board, x_index, y_index):
    row_length = int(math.sqrt(len(board)))
    living_neighbors = _sum_neighbors(board, x_index, y_index)
    cell_status = int(board[_get_linear_index(x_index, y_index, row_length)])

    if cell_status and living_neighbors in [2, 3]:
        return '1'
    if not cell_status and living_neighbors == 3:
        return '1'
    return '0'


def _sum_neighbors(board, x_index, y_index):
    row_length = int(math.sqrt(len(board)))
    max_index = row_length - 1
    sum_neighbors = 0
    for x_neighbor in [x_index - 1, x_index, x_index + 1]:
        x_neighbor = _wrap_index(x_neighbor, max_index)
        for y_neighbor in [y_index - 1, y_index, y_index + 1]:
            y_neighbor = _wrap_index(y_neighbor, max_index)
            if not (x_neighbor == x_index and y_neighbor == y_index):
                sum_neighbors += int(board[_get_linear_index(x_neighbor, y_neighbor, row_length)])
    return sum_neighbors


def _wrap_index(index, max_index):
    if index < 0:
        return max_index
    if index > max_index:
        return 0
    return index


def _get_linear_index(x_index, y_index, row_length):
    return y_index * row_length + x_index


def _filter_empty_states():
    filtered_start = []
    filtered_end = []
    boards_filtered = 0
    for board_index in range(len(dataset['start'])):
        start_array = np.array(list(dataset['start'][board_index])).astype(int)
        end_array = np.array(list(dataset['end'][board_index])).astype(int)
        sum_start = sum(start_array)
        sum_end = sum(end_array)
        if(sum_start == 0 or sum_end == 0 or sum_start == len(start_array) or sum_end == len(end_array)):
            boards_filtered += 1
        else:
            filtered_start.append(dataset['start'][board_index])
            filtered_end.append(dataset['end'][board_index])
    dataset['start'] = filtered_start
    dataset['end'] = filtered_end
    print('Filtered {} empty boards...'.format(boards_filtered))
