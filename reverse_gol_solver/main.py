import click

from .utils import analyzer, data_generator, renderer
from .network_models import forward_cnn, forward_cnn_reducing


@click.group()
def cli():
    """Utility to train reverse GOL model"""
    pass


@cli.command()
@click.argument('index', type=click.INT)
@click.argument('state')
@click.argument('board_width', type=click.INT)
@click.option('--verbose/--quiet', default=True)
def render_test_board(index: int, state, board_width, verbose):
    print('Fetching board...')
    data = data_generator.get_dataset(board_width)
    if verbose:
        print('Start state count: ', len(data.get(state)))
        print('Board for index: ', index)
        renderer.render_board(data.get(state)[index])
        analyzer.build_reverse_hist(data)
        print('Done.')


@cli.command()
@click.argument('board_width', type=click.INT)
@click.argument('dataset_size', type=click.INT)
def save_training_data(board_width, dataset_size):
    print('Saving data...')
    data_generator.save_training_data(board_width, dataset_size)
    renderer.render_board(data_generator.dataset.get('start')[0])
    print()
    renderer.render_board(data_generator.dataset.get('end')[0])


if __name__ == '__main__':
    print('Starting main...')
    # render_test_board()
    # save_training_data()
    forward_cnn_reducing.run()
