# import cv2
import numpy as np
import sys
import torch
from torch.utils.data import DataLoader, TensorDataset
import torch.nn.functional as F
import pickle
from PIL import Image

# BOARD_HEIGHT = 3
# BOARD_WIDTH = 3
BOARD_HEIGHT = 100
BOARD_WIDTH = 100

examples = torch.load('/Users/vytasbradunas/Dropbox/VU Data Science Studies/Data Mining/reverse_gol_solver/reverse_gol_solver/network_models/train.data.batch')
validation_examples = torch.load('/Users/vytasbradunas/Dropbox/VU Data Science Studies/Data Mining/reverse_gol_solver/reverse_gol_solver/network_models/validation.data.batch')

trainloader = torch.utils.data.DataLoader(examples, batch_size=8,
                                          shuffle=True)

validloader = torch.utils.data.DataLoader(examples, batch_size=8,
                                          shuffle=True)


# cv2.namedWindow("game", cv2.WINDOW_NORMAL)


def _load_data_pickles():
    with open('ending_states.pickle', 'rb') as pickle_end_file:
        np_ending = pickle.load(pickle_end_file)
    with open('starting_states.pickle', 'rb') as pickle_start_file:
        np_starting = pickle.load(pickle_start_file)
    with open('ending_states_validation.pickle', 'rb') as pickle_end_file:
        np_ending_validation = pickle.load(pickle_end_file)
    with open('starting_states_validation.pickle', 'rb') as pickle_start_file:
        np_starting_validation = pickle.load(pickle_start_file)
    np_ending, np_starting, np_ending_validation, np_starting_validation = map(
        torch.tensor, (np_ending, np_starting, np_ending_validation, np_starting_validation))
    return (np_starting.float(), np_ending.float(), np_starting_validation.float(), np_ending_validation.float())


def _get_data(batch_size):
    data = _load_data_pickles()
    # data = _load_data_pickles_reverse()
    # Forward prediction for now
    train_ds = TensorDataset(data[0], data[1])  # starting states, ending states
    valid_ds = TensorDataset(data[2], data[3])
    return (
        DataLoader(train_ds, batch_size=batch_size, shuffle=True),
        DataLoader(valid_ds, batch_size=batch_size * 2),  # double batch size because validation is more efficient
    )


class GoL(torch.nn.Module):
    def __init__(self):
        super(GoL, self).__init__()
        self.conv1 = torch.nn.Conv2d(1, 1, (3, 3), padding=1)
        self.fc1 = torch.nn.Linear(BOARD_HEIGHT*BOARD_WIDTH, BOARD_HEIGHT*BOARD_WIDTH)

    def forward(self, x):
        x = self.conv1(x)
        x = self.fc1(x.view(-1, BOARD_HEIGHT*BOARD_WIDTH))
        x = F.selu(x)
        return x.view(-1,1,BOARD_HEIGHT,BOARD_WIDTH)


def get_accuracy(output, target):
    return sum(((output.view(-1) >= 0.5).int() == target.view(-1)).int()), len(target.view(-1))

# trainloader, validationloader = _get_data(batch_size=8)
gol = GoL()

criterion = torch.nn.MSELoss()
optimizer = torch.optim.Adam(gol.parameters(), lr=0.001)
for epoch in range(32):
    running_loss = 0.0
    running_wins = 0
    running_total = 0
    for i, data in enumerate(trainloader):
        last_frame, target_ = data
        # target_ = target_.view(-1, 5, 5)[:, 1:4, 1:4].reshape(-1, 9).view(-1,1,BOARD_HEIGHT,BOARD_WIDTH).to(torch.float32)
        # last_frame = last_frame.view(-1,1,5,5).to(torch.float32)
        target_ = target_.view(-1,1,BOARD_HEIGHT,BOARD_WIDTH).to(torch.float32)
        last_frame = last_frame.view(-1,1,BOARD_HEIGHT,BOARD_WIDTH).to(torch.float32)
        input_ = last_frame
        
        optimizer.zero_grad()

        output_ = gol(input_)
        loss = criterion(output_, target_) * 56 # loss on non-aliver is penalized more
        with torch.no_grad():
            wins, total_cases = get_accuracy(output_, target_)
        loss.backward()
        optimizer.step()

        running_wins += wins
        running_total += total_cases
        running_loss += loss.item()
        if i % 20 == 0:
            print('[%d, %5d] loss: %.3f' % (epoch + 1, i + 1, running_loss /20))
            accuracy = running_wins.float() / running_total
            print(accuracy)
            running_loss = 0.0
            running_wins = 0
            running_total = 0
        last_frame = data

    for i, data in enumerate(validloader):
        last_frame, target_ = data
        # target_ = target_.view(-1, 5, 5)[:, 1:4, 1:4].reshape(-1, 9).view(-1,1,BOARD_HEIGHT,BOARD_WIDTH).to(torch.float32)
        # last_frame = last_frame.view(-1,1,5,5).to(torch.float32)
        target_ = target_.view(-1,1,BOARD_HEIGHT,BOARD_WIDTH).to(torch.float32)
        last_frame = last_frame.view(-1,1,BOARD_HEIGHT,BOARD_WIDTH).to(torch.float32)
        input_ = last_frame
        with torch.no_grad():
            output_ = gol(input_)
            with torch.no_grad():
                wins, total_cases = get_accuracy(output_, target_)

            running_wins += wins
            running_total += total_cases
            if i % 20 == 0:
                accuracy = running_wins.float() / running_total
                print('Validation accuracy: ', accuracy)
                running_loss = 0.0
                running_wins = 0
                running_total = 0
            last_frame = data

# distrib = torch.distributions.Bernoulli(0.5)
# board = distrib.sample((BOARD_HEIGHT,BOARD_WIDTH)).view(1,1,BOARD_HEIGHT,BOARD_WIDTH)
# board = board.to(torch.float32).to('cuda')

# while True:
#     board_array = np.int8(board.clone().cpu().view(BOARD_HEIGHT,BOARD_WIDTH).detach()) * 255
#     img = Image.fromarray(board_array).convert('RGB')
#     img = np.array(img)
#     cv2.imshow("game", img)

#     newboard = gol(board)
    
#     q = cv2.waitKey(200)
#     if q == 113: # 'q'
#         cv2.destroyAllWindows()
#         break
#     if q == 114: # 'r'
#         distrib = torch.distributions.Bernoulli(0.5)
#         board = distrib.sample((BOARD_HEIGHT,BOARD_WIDTH)).view(1,1,BOARD_HEIGHT,BOARD_WIDTH)
#         board = board.to(torch.float32).to('cuda')
#         newboard = board
#     board = newboard