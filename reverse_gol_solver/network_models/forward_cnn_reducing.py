import numpy as np
import torch
from torch.utils.data import DataLoader, TensorDataset
import torch.nn.functional as F
from torch import nn, optim
# from torchvision import transforms
import pickle

from ..utils import renderer
from ..utils import model_scaler

# https://towardsdatascience.com/a-comprehensive-introduction-to-different-types-of-convolutions-in-deep-learning-669281e58215

ORIGINAL_SIZE = 25
# DIRECTION = 'FORWARD'
DIRECTION = 'BACKWARD'
EVALUATION_SIZE = ORIGINAL_SIZE - 2


def preprocess(x, y):
    return x.view(-1, 1, ORIGINAL_SIZE, ORIGINAL_SIZE), y  # second two are the dimensions of our "images"


class WrappedDataLoader:
    def __init__(self, dl, func):
        self.dl = dl
        self.func = func

    def __len__(self):
        return len(self.dl)

    def __iter__(self):
        batches = iter(self.dl)
        for b in batches:
            yield (self.func(*b))


class Lambda(nn.Module):
    def __init__(self, func):
        super().__init__()
        self.func = func

    def forward(self, x):
        return self.func(x)


def _get_data(batch_size):
    data = _load_data_pickles()
    train_ds = TensorDataset(data[0], data[1])  # starting states, ending states
    valid_ds = TensorDataset(data[2], data[3])
    return (
        DataLoader(train_ds, batch_size=batch_size, shuffle=True),
        DataLoader(valid_ds, batch_size=batch_size * 2),  # double batch size because validation is more efficient
    )


def _load_data_pickles():
    if(DIRECTION == 'BACKWARD'):
        return _load_data_pickles_reverse()
    print('Playing game forwards...')
    ending_file = 'ending_states'
    starting_file = 'starting_states'
    ending_validation_file = 'ending_states_validation'
    starting_validation_file = 'starting_states_validation'
    ending_file, starting_file, ending_validation_file, starting_validation_file = map(
        _append_board_size_to_file, (ending_file, starting_file, ending_validation_file, starting_validation_file))
    with open(ending_file, 'rb') as pickle_end_file:
        np_ending = pickle.load(pickle_end_file)
    with open(starting_file, 'rb') as pickle_start_file:
        np_starting = pickle.load(pickle_start_file)
    with open(ending_validation_file, 'rb') as pickle_end_file:
        np_ending_validation = pickle.load(pickle_end_file)
    with open(starting_validation_file, 'rb') as pickle_start_file:
        np_starting_validation = pickle.load(pickle_start_file)
    print('Data found for {} sized boards. {} total observations...'.format(ORIGINAL_SIZE, len(np_ending)))
    np_ending, np_starting, np_ending_validation, np_starting_validation = map(
        torch.tensor, (np_ending, np_starting, np_ending_validation, np_starting_validation))
    np_ending = np_ending.view(-1, ORIGINAL_SIZE, ORIGINAL_SIZE)[:, 1:1 + EVALUATION_SIZE, 1:1 + EVALUATION_SIZE].reshape(-1, pow(EVALUATION_SIZE, 2))
    np_ending_validation = np_ending_validation.view(-1, ORIGINAL_SIZE, ORIGINAL_SIZE)[:, 1:1 + EVALUATION_SIZE, 1:1 + EVALUATION_SIZE].reshape(-1, pow(EVALUATION_SIZE, 2))
    return (np_starting.float(), np_ending.float(), np_starting_validation.float(), np_ending_validation.float())


def _append_board_size_to_file(file_name):
    board_size = str(ORIGINAL_SIZE)
    return './data/' + file_name + '_' + board_size + 'x' + board_size + '.pickle'


def _load_data_pickles_reverse():
    print('Playing game in reverse...')
    ending_file = 'starting_states'
    starting_file = 'ending_states'
    ending_validation_file = 'starting_states_validation'
    starting_validation_file = 'ending_states_validation'
    ending_file, starting_file, ending_validation_file, starting_validation_file = map(
        _append_board_size_to_file, (ending_file, starting_file, ending_validation_file, starting_validation_file))
    with open(ending_file, 'rb') as pickle_end_file:
        np_ending = pickle.load(pickle_end_file)
    with open(starting_file, 'rb') as pickle_start_file:
        np_starting = pickle.load(pickle_start_file)
    with open(ending_validation_file, 'rb') as pickle_end_file:
        np_ending_validation = pickle.load(pickle_end_file)
    with open(starting_validation_file, 'rb') as pickle_start_file:
        np_starting_validation = pickle.load(pickle_start_file)
    print('Data found for {} sized boards. {} total observations...'.format(ORIGINAL_SIZE, len(np_ending)))
    np_ending, np_starting, np_ending_validation, np_starting_validation = map(
        torch.tensor, (np_ending, np_starting, np_ending_validation, np_starting_validation))
    np_ending = np_ending.view(-1, ORIGINAL_SIZE, ORIGINAL_SIZE)[:, 1:1 + EVALUATION_SIZE, 1:1 + EVALUATION_SIZE].reshape(-1, pow(EVALUATION_SIZE, 2))
    np_ending_validation = np_ending_validation.view(-1, ORIGINAL_SIZE, ORIGINAL_SIZE)[:, 1:1 + EVALUATION_SIZE, 1:1 + EVALUATION_SIZE].reshape(-1, pow(EVALUATION_SIZE, 2))
    return (np_starting.float(), np_ending.float(), np_starting_validation.float(), np_ending_validation.float())


def get_model():
    # return Mnist_Logistic()
    return nn.Sequential(
        # nn.Conv2d(1, 1, kernel_size=3, padding=0),
        nn.Conv2d(1, 16, kernel_size=1, padding=0),
        nn.SELU(),
        nn.Conv2d(16, 16, kernel_size=3, padding=0),
        nn.SELU(),
        nn.Conv2d(16, 1, kernel_size=1, padding=0),
        nn.SELU(),
        Lambda(lambda x: x.view(-1, pow(EVALUATION_SIZE, 2))),
        nn.Linear(pow(EVALUATION_SIZE, 2), pow(EVALUATION_SIZE, 2)),
        nn.SELU(),
        Lambda(lambda x: x.view(x.size(0), -1)))


def loss_batch(model, loss_func, xb, yb, opt=None):
    if opt is not None:
        opt.zero_grad()
    loss = loss_func(model(xb), yb) * 56

    if opt is not None:
        loss.backward()
        opt.step()

    return loss.item(), len(xb)


def get_accuracy(model, xb, yb):
    return sum(((model(xb).view(-1) >= 0.5).int() == yb.view(-1)).int()), len(yb.view(-1))


def visualize_model(valid_dl, model):
    for xb, yb in valid_dl:
        print("Results:")
        print(model(xb).tolist())
        print(yb.tolist())
        print()


def fit(epochs, model, loss_func, opt, train_dl, valid_dl):
    for epoch in range(epochs):
        model.train()
        for xb, yb in train_dl:
            loss_batch(model, loss_func, xb, yb, opt)

        model.eval()
        with torch.no_grad():
            losses, nums = zip(
                *[loss_batch(model, loss_func, xb, yb) for xb, yb in valid_dl])
            wins, total_cases = zip(
                *[get_accuracy(model, xb, yb) for xb, yb in valid_dl])
        val_loss = np.sum(np.multiply(losses, nums)) / np.sum(nums)
        accuracy = np.sum(wins) / np.sum(total_cases)

        print(epoch, val_loss, accuracy)
        if(epoch > 10 and accuracy == 1.0):
            model_scaler.scale_model(model, ORIGINAL_SIZE, EVALUATION_SIZE, 25)


def run():
    print('Forward cnn reducing...')
    # loss_func = F.cross_entropy
    # loss_func = nn.MSELoss()
    loss_func = nn.BCEWithLogitsLoss()
    lr = 0.005  # learning rate
    epochs = 2000  # how many epochs to train for
    train_dl, valid_dl = _get_data(batch_size=64)
    train_dl = WrappedDataLoader(train_dl, preprocess)
    valid_dl = WrappedDataLoader(valid_dl, preprocess)
    model = get_model()
    # opt = optim.SGD(model.parameters(), lr=lr)
    opt = torch.optim.Adam(model.parameters(), lr=lr)
    fit(epochs, model, loss_func, opt, train_dl, valid_dl)
    print('Done.')
#     preds = model(xb)  # predictions
# preds[0], preds.shape
# yb = y_train[0:bs]
# print(loss_func(preds, yb))
# print(preds[0], preds.shape)
# def accuracy(out, yb):
#     preds = torch.argmax(out, dim=1)
#     return (preds == yb).float().mean()
# Let’s check the accuracy of our random model, so we can see if our accuracy improves as our loss improves.

# print(accuracy(preds, yb))
